from django.shortcuts import render
from django.http import HttpRequest, HttpResponse

# Create your views here.
def lesson_1_1(request: HttpRequest) -> HttpResponse:
    return render(request, "lesson_11/index.html")

def lesson_1(request: HttpRequest) -> HttpResponse:
    return HttpResponse("Python Django")